/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static javax.ws.rs.client.Entity.xml;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

@WebServlet(name = "GetGeoIpWs2", urlPatterns = {"/GetGeoIpWs2"})
public class GetGeoIpWs2 extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String ipAddr = request.getParameter("ip");
        //PrintWriter out = response.getWriter();
        try (PrintWriter out = response.getWriter()) {
           
            try{
            MessageFactory mFac = MessageFactory.newInstance();
            // สร้าง SOAPMessage จาก MessageFactory
            SOAPMessage message = mFac.createMessage();
            // ดึงส่วนของ SOAPBody จาก SOAPMessage
            SOAPBody body = message.getSOAPBody();
            // สร้าง SOAPFactory เพื่อเตรียมสร้างรายละเอียดของ SOAP
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            // สร้าง object Name (javax.xml.soap.Name)
            // ที่มี local name คือ GetGeoIP มี prefix คือ ns และ
            // มี namespace คือ http://www.webservicex.net/
            String prefix = "ns";
            String namespace = "http://www.webservicex.net/";
            Name opName = soapFactory.createName("GetGeoIP",
                    prefix, namespace);
            // ใส่ opName เข้าไปใน SOAPBody
            SOAPBodyElement opElem = body.addBodyElement(opName);
            // สร้าง SOAPElement ของ IpAddress
            SOAPElement ip = opElem.addChildElement(
                    soapFactory.createName("IPAddress", prefix, namespace));
            ip.addTextNode(ipAddr); // เพิ่ม ip ที่ต้องการค้นหา
            // เพิ่มส่วนการกำหนด SOAPAction
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");
            // แสดงข้อความ SOAP Request
//        	displayMessage(message, out);
            // สร้าง SOAPConnection เพื่อเรียกใช้เว็บเซอร์วิส และรับค่าผลลัพธ์ด้วย SOAPMessage
            SOAPConnection soapConn
                    = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
            //out.println(resp);
            // แสดงข้อความ SOAP Response
            StringWriter write = new StringWriter();
            String get = displayMessage(resp, write);
            
            
            try {
            
           
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(get));
            Document doc = parser.parse(is);           
            Element root = doc.getDocumentElement();
       
            if(root.hasChildNodes()){
              org.w3c.dom.Node nodeBody = root.getFirstChild();
             
                if(nodeBody.hasChildNodes()){
                      org.w3c.dom.Node nodeGetGeoIPResponse = nodeBody.getFirstChild();
                      
                     if(nodeGetGeoIPResponse.hasChildNodes()){
                          org.w3c.dom.Node nodeGetIPResult = nodeGetGeoIPResponse.getFirstChild();
                          
                         if(nodeGetIPResult.hasChildNodes()){
                             Element eElement = (Element) nodeGetIPResult;
                            String getInput = eElement.getElementsByTagName("IP").item(0).getTextContent();
                            String country = eElement.getElementsByTagName("CountryName").item(0).getTextContent();
                            
                            out.println("<P><font color=\"blue\">" + getInput +"</font> is in <font color=\"blue\">" 
                                    + country + "</font></P>");
                         }
                     }
                }
            }
            
            
            }   catch (Exception e) {
            out.println("error ");
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        }
           
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    	private String displayMessage(SOAPMessage message, StringWriter outWriter) {
            String finalstring = null;
    	try {
        	TransformerFactory tFac = TransformerFactory.newInstance();
        	Transformer transformer = tFac.newTransformer();
                
                //StringWriter outWriter = new StringWriter();
               
               Source src =  message.getSOAPPart().getContent();
               StreamResult result = new StreamResult(outWriter);
 	       transformer.transform(src, result);
               StringBuffer sb = outWriter.getBuffer(); 
               finalstring = sb.toString();
              
    	} catch (Exception ex) {
        	ex.printStackTrace();
    	} 
            return finalstring;
	}
}
