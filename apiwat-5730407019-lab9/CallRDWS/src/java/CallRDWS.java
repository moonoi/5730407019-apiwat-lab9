
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author HP
 */
public class CallRDWS {

    public void msgEnvelope(String[] args) throws Exception {

        SOAPConnectionFactory soapConnectionFactory
                = SOAPConnectionFactory.newInstance();
        SOAPConnection connection
                = soapConnectionFactory.createConnection();
        MessageFactory messageFactory
                = MessageFactory.newInstance();
        String serviceUrl = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
        String prefix = "ns";
        // Create a message
        SOAPMessage message
                = messageFactory.createMessage();

        SOAPHeader header = message.getSOAPHeader();
        SOAPBody body = message.getSOAPBody();
        header.detachNode();
        SOAPFactory soapFactory
                = SOAPFactory.newInstance();
        SOAPBodyElement checkPin
                = body.addBodyElement(soapFactory.createName("ServiceTIN",
                        prefix, serviceUrl));

        SOAPElement username
                = checkPin.addChildElement(
                        soapFactory.createName("username", prefix,
                                serviceUrl));
        username.addTextNode("anonymous");

        SOAPElement password
                = checkPin.addChildElement(
                        soapFactory.createName("password", prefix,
                                serviceUrl));
        password.addTextNode("anonymous");

        SOAPElement pin
                = checkPin.addChildElement(
                        soapFactory.createName("TIN", prefix,
                                serviceUrl));
        pin.addTextNode("3841200369685");

        MimeHeaders hd = message.getMimeHeaders();
        hd.addHeader("SOAPAction",
                "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");

        message.saveChanges();
        System.out.println("REQUEST:");
        //Display Request Message
        displayMessage(message);

        System.out.println("\n\n");
        //add code below for trust x.509 ceritficate
        XTrustProvider.install();
        SOAPConnection conn
                = SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message,
                "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx?WSDL");

        System.out.println("RESPONSE:");
        //Display Response Message
        displayMessage(response);
    }
    public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }

    public static void main(String[] args) throws Exception {
        CallRDWS clientApp = new CallRDWS();
        clientApp.msgEnvelope(args);
    }
}
